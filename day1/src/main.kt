//open class  Person (var name:String, var surname:String, var gpa:Double){
//    constructor(name:String, surname:String): this(name,surname,0.0){
//    }
//   open fun getDetails(): String {
//        return  "$name $surname has score $gpa"
//
//    }
//}
//
//class  Student ( name:String,  surname:String, gpa:Double,var department:String) : Person(name,surname,gpa){
//override fun getDetails():String {
//    return "$name $surname has score $gpa and study in $department"
//}
//}
//
//fun main(args: Array<String>){
//    val no1:Person = Person ("Prawfon","Thitimethiyakun")
//    val no2:Person = Person (surname="Prawfon",name="Praw")
//    val no3:Person = Person (name="Praw",surname="Prawfon")
//    val no4:Student = Student ("Prawfon","Thitimethiyakun" ,3.00,"hotel")
//    println(no1.getDetails())
//    println(no2.getDetails())
//    println(no3.getDetails())
//    println(no4.getDetails())
//}

//abstract class Person(var name: String, var surname: String, var gpa: Double) {
//    constructor(name: String, surname: String) : this(name, surname, 0.0) {
//
//    }
//    abstract fun goodboy():Boolean
//    open fun getDetail(): String {
//        return "$name $surname has score $gpa"
//    }
//}
//
//class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
//    override fun goodboy(): Boolean {
//        return  gpa>20
//    }
//    override fun getDetail(): String {
//        return super.getDetail() + "and study in $department"
//    }
//}
//
//fun main(args: Array<String>) {
//    val no5: Student = Student("bb", "accb", 5.00, "dd")
//    println(no5.getDetail())
//}

//abstract class Animal(var name: String, var noOfLegs: Int, var food: String) {
//
//    abstract fun getSound(): String
//
//}
//
//class Lion(name: String, noOfLegs: Int, food: String) : Animal(name, noOfLegs, food) {
//    override fun getSound(): String {
//        return "$name  have $noOfLegs Legs and sound "
//    }
//
//}
//
//class Dog(name: String, noOfLegs: Int, food: String) : Animal(name, noOfLegs, food) {
//    override fun getSound(): String {
//        return "$name  have $noOfLegs Legs and sound"
//    }
//
//}
//
//fun main(args: Array<String>) {
//    val no1: Dog = Dog("Dog", 4,"pork")
//    val no2: Lion = Lion("Lion" , 4 ,"beef")
//    println(no1.getSound())
//            println(no2.getSound())
//}

enum class Color {
    RED , GREEN , BLUE
}

data class Teachers(var name: String , var courseName : String ,var shirtColor :Color)

fun main(args: Array<String>) {
    var teacher1 : Teachers = Teachers("ffff","abc",Color.BLUE)
    var teacher2 : Teachers = Teachers("ffff","cde",Color.GREEN)
    var teacher3 : Teachers = Teachers("aaa","vvv",Color.RED)
    println(teacher1)
    println(teacher2)
    println(teacher3)
}